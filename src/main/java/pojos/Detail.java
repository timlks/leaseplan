package pojos;

import lombok.Data;

@Data
public class Detail {

	private Boolean error;
	private String message;
	private String requested_item;
	private String served_by;

	/**
	 * 
	 */
	public Detail() {
		super();
	}

}
