package pojos;

import lombok.Data;

@Data
public class ErrorMessage {

	private Detail detail;

	/**
	 * 
	 */
	public ErrorMessage() {
		super();
	}

}
