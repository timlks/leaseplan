package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import pojos.ErrorMessage;
import pojos.Product;
import starter.helpers.CarsAPI;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.contains;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class SearchStepDefinitions {

	@Steps
	public CarsAPI carsAPI;

	@When("the user calls endpoint {string}")
	public void the_user_calls_endpoint(String endpoint) {
		SerenityRest.when().get(endpoint);
	}

	@Then("a {int} response is returned")
	public void a_response_is_returned(Integer code) {
		restAssuredThat(response -> response.statusCode(code));
	}

	@Then("the schema is validated")
	public void the_schema_is_validated() {
		SerenityRest.then().assertThat().body(matchesJsonSchemaInClasspath("schemas/products.json"));
	}

	@Then("the a {string} error is returned")
	public void the_a_error_is_returned(String error) {
		ErrorMessage errorMessage = SerenityRest.then().extract().response().as(ErrorMessage.class);
		assertThat(errorMessage.getDetail().getMessage()).isEqualTo(error);
	}

}
