package starter.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

//@formatter:off
	@RunWith(CucumberWithSerenity.class)
	@CucumberOptions(
			features = "src/test/resources/features", 
			glue = { "starter.stepdefinitions" }, 
			tags = "@test",
			plugin = { "pretty"}
			)
//@formatter:on

public class TestRunner {
}
