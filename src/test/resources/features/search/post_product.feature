Feature: Search for the product

  ### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
  ### Available products: "apple", "mango", "tofu", "water"
  ### Prepare Positive and negative scenarios
  
  @test
  Scenario Outline: 
    When the user calls endpoint "<Endpoint>"
    Then a 200 response is returned
    And the schema is validated

    Examples: 
      | Endpoint                                                       |
      | https://waarkoop-server.herokuapp.com/api/v1/search/test/apple |
      | https://waarkoop-server.herokuapp.com/api/v1/search/test/mango |
      | https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu  |
      | https://waarkoop-server.herokuapp.com/api/v1/search/test/water |

  Scenario: 
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then a 404 response is returned
    And the a "Not found" error is returned
